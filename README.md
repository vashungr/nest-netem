# NeST-NetEm

Description of `NetEm` in `NeST`.   
Collection of supported API(feature) in `NeST` and APIs do not support in `NeST`

## NetEm

`NetEm` is an enhancement of the Linux traffic control facilities that allow to add `delay, packet loss, curruption, duplication`
and more other characteristics to packets outgoing from a selected network interface.  
`NetEm` is built using the existing Quality Of Service (QOS) and Differentiated Services (diffserv) facilities in the Linux kernel



## NetEM API in NeST (Supported)

***Note:*** User has to access using admin previlege : `sudo su` or `sudo <cmd>`

### 1. Adding Delay    

Command for adding delay  

    tc qdisc add dev {eth-name} root netem delay {delay_rate} 
    
In NeST, we can add delay as given below,

```python        
eth1,eth2) = connect(h1,h2)
eth1.set_address("192.168.1.1/24")
eth2.set_address("192.168.1.2/24")

eth1.set_delay("")
```


### 2. Adding Corruption  


Command for adding corruption, 
 
    tc qdisc add dev eth1 root netem corrupt 20% 50
    
In `NeST`, we can add ***corruption*** as given below: 

```python        
eth1,eth2) = connect(h1,h2)
eth1.set_address("192.168.1.1/24")
eth2.set_address("192.168.1.2/24")

eth1.set_packet_corruption("20%","50")
```


**API for packet corruption**  

```python
    class Interface:  
        def set_packet_corruption(self, corrupt_rate, correlation_rate):
            
            self._veth_end.set_structure()
            loss_parameter = {"corrupt": corrupt_rate, "": correlation_rate}
            self._veth_end.change_qdisc("11:", "netem", **loss_parameter)

```


### 3. Adding Packet loss

Command for adding packet loss,  

    tc qdisc add dev {eth-name} root netem loss {delay_rate} 
    
In `NeST`, we can add ***packet loss*** as given below: 

```python

eth1,eth2) = connect(h1,h2)    
eth1.set_address("192.168.1.1/24")  
eth2.set_address("192.168.1.2/24")  

eth1.set_packet_corruption("20%","50")
```


**API for packet loss**  
```python
    class Interface:  
        def set_packet_loss(self, loss_rate, correlation_rate):
           
            self._veth_end.set_structure()
            loss_parameter = {"loss": loss_rate, "": correlation_rate}
            self._veth_end.change_qdisc("11:", "netem", **loss_parameter)

```

## NetEm feature not supported in NeST

The following features aren't supported in `NeST`,   
   1. Duplicate packet 
   2. Packet Reordering
   3. Packet distribution

### 1. Adding duplicate packet 

Adding duplicate packet command:

    tc qdisc add dev eth1 root netem duplicate 20%  


I added the API on `class Interface`, code is given below  

```python

class Interface:
    ...
    def set_packet_duplication(self, duplicate_rate, correlation_rate):
       
        self._veth_end.set_structure()
        duplicate_parameter = {"duplicate": duplicate_rate, "":correlation_rate}
        self._veth_end.change_qdisc("11:","netem", **duplicate_parameter)
```


# Usage

## Important  

1. Working directory: `nest/`
    Make sure your working directory is in `nest/`.  


2. Admin previlege: `sudo su`


### Procedure  

**1. Get into python command line by typing** `python3`  
   
```python
        Python 3.9.7 (default, Sep 10 2021, 14:59:43) 
        [GCC 11.2.0] on linux
        Type "help", "copyright", "credits" or "license" for more information.
        >>> 
```  

**2. Create a simple network topology: Poin-to-point topology** 
   
```python
   Python 3.9.7 (default, Sep 10 2021, 14:59:43) 
   [GCC 11.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from nest.topology import *
    >>> from nest import config
    >>> config.set_value("delete_namespaces_on_termination", False)
    >>> config.set_value("assign_random_names", False)
    >>> 
    >>> h1 = Node("n1")
    >>> h2 = Node("n2")
    >>> (eth1, eth2) = connect(h1, h2)
    >>> eth1.set_address("192.168.1.1/24")
    >>> eth2.set_address("192.168.1.2/24")

   ```
  

**3. Add duplicate packet using** `set_packet_duplication` **API**  
   
```python

    >>> eth2.set_packet_duplication("20%")

```

**4. Now open terminal for two host `n1` and `n2`  and ping from one host to another and check if the duplicate packet is present during packet transmission.**   

Get into host `n1` terminal and host `n2` terminal:  

    $ ip netns exec n1 bash      |      $ ip netns exec n2 bash


**5. Ping host** `n2` **(192.168.1.2) from host** `n1`   

```python


    root@shung-linux:~# ping 192.168.1.2
    PING 192.168.1.2 (192.168.1.2) 56(84) bytes of data.
    64 bytes from 192.168.1.2: icmp_seq=1 ttl=64 time=0.070 ms
    64 bytes from 192.168.1.2: icmp_seq=2 ttl=64 time=0.047 ms
    64 bytes from 192.168.1.2: icmp_seq=2 ttl=64 time=0.051 ms (DUP!)
    64 bytes from 192.168.1.2: icmp_seq=3 ttl=64 time=0.048 ms
    64 bytes from 192.168.1.2: icmp_seq=4 ttl=64 time=0.063 ms
    64 bytes from 192.168.1.2: icmp_seq=4 ttl=64 time=0.069 ms (DUP!)
    64 bytes from 192.168.1.2: icmp_seq=5 ttl=64 time=0.072 ms
    64 bytes from 192.168.1.2: icmp_seq=5 ttl=64 time=0.080 ms (DUP!)
    64 bytes from 192.168.1.2: icmp_seq=6 ttl=64 time=0.076 ms
    64 bytes from 192.168.1.2: icmp_seq=7 ttl=64 time=0.072 ms
    64 bytes from 192.168.1.2: icmp_seq=8 ttl=64 time=0.040 ms
    64 bytes from 192.168.1.2: icmp_seq=9 ttl=64 time=0.071 ms
    64 bytes from 192.168.1.2: icmp_seq=10 ttl=64 time=0.085 ms
    ^C
    --- 192.168.1.2 ping statistics ---
    10 packets transmitted, 10 received, +3 duplicates, 0% packet loss, time 9224ms
    rtt min/avg/max/mdev = 0.040/0.064/0.085/0.013 ms

```

From above result we know that duplicate packet is present in transmission which is expected. Thus duplicate packet API is working.  


# ISSUE  

Change working directory from `nest/` to any directory and do the same thing as it is done above. You will get an error as given below.  
### ERROR:

```python

AttributeError: `Interface' object has no attribute 'set_packet_duplication'

```

### Debugging Error:  

Attribute `set_packet_duplication` is declared but couldn`t read the API.

Currently your working directory will be `nest/examples/tutorials/config-options/`  
Get into the python3 commandline by typing `python3` on terminal:  
and check all the available `class Interface` method.  

Command to check all the available class methods  
```python
print(dir(Interface))
```
```python
        > python3
        Python 3.9.7 (default, Sep 10 2021, 14:59:43) 
        [GCC 11.2.0] on linux  
        Type "help", "copyright", "credits" or "license" for more information.  

        > from nest.topology import *
        > print(dir(Interface))
        ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', 
         '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__',  
         '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '
         __sizeof__', '__str__', '__subclasshook__', '__weakref__', '_create_and_mirred_to_ifb', 'address',
          'disable_ip_dad', 'disable_offload', 'enable_mpls', 'enable_offload', 'get_address', 'get_qdisc', 
          'id', 'ifb_id', 'is_mpls_enabled', 'mtu', 'name', 'node_id', 'pair', 'set_address', 'set_attributes',
           'set_bandwidth', 'set_delay', 'set_mode', 'set_packet_corruption', 'set_packet_loss', 'set_qdisc',
         'subnet']
```

***Note*** : `set_packet_duplication(...)` is not available here.  


But if you change your working directory to `nest/` and check - all the available 
methods for `class Interface`:  

```python
        > python3
        Python 3.9.7 (default, Sep 10 2021, 14:59:43) 
        [GCC 11.2.0] on linux
        Type "help", "copyright", "credits" or "license" for more information.

        > from nest.topology import *
        > print(dir(Interface))
         ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__',    '__eq__', '__format__',  
         '__ge__', '__getattribute__', '__gt__',     '__hash__', '__init__', '__init_subclass__',  
         '__le__', '__lt__',     '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__',    
          '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__',   
         '__weakref__', '_create_and_mirred_to_ifb', 'address',    'disable_ip_dad', 'disable_offload',   
         'enable_mpls', 'enable_offload',   'get_address', 'get_qdisc', 'id', 'ifb_id', 'is_mpls_enabled',  
          'mtu',   'name', 'node_id', 'pair', 'set_address', 'set_attributes',   'set_bandwidth', 'set_delay',   
          'set_mode', 'set_packet_corruption',   'set_packet_duplication', 'set_packet_loss', 'set_qdisc', 'subnet']  

```

***NOTE:*** `set_packet_duplication` method is available here. [ Checking in `nest/` directory.]


***NOTE:*** `set_packet_duplication` method is available here.

### Possible Problem (not sure): 


Could be an Abstraction, since I couldn't add new API to the `class Interface`, 
couldn't read my code even if declared inside `class Interface`
```python 
class Interface:
``` 


We know that the only difference between `corrupt` and `duplicate packet` command is the name itself when given command -   
i.e.   

For `corrupt`: 
 
        tc qdisc add dev eth1 root netem corrupt 20%    

For `duplicate`:  

         tc qdisc add dev eth1 root netem duplicate 20%  

They are declared inside `nest.topology.interface.Interface`,  
 `set_packet_corruption(...)` fucntion is recognised when declared
 ```python
 eth1.set_packet_corruption("20%")
 ```  
  whereas `set_packet_duplication(...)` function is not recognised even though they are declared inside the 
  `class Interface`

```python
eth1.set_packet_corruption("20%")
```  

whereas ***set_packet_duplication(...)*** function is not recognised even though they are declared inside the `class Interface`


# SOLUTION TO ABOVE PROBLEM

  The above problem occurs due to multiple nitk-nest installation on my machine. 
  So I remove one of the older nitk-nest version and keep the 0.3 version and try to run the same program 
  as I did in the above.

  
  # Test case:
  ### Testcase
  Also I write a test case for the given above API:  

  ```python

    import unittest
    from nest.topology import Node, connect
    from nest.experiment import Experiment, Flow
    from nest.clean_up import delete_namespaces
    from nest.topology_map import TopologyMap
    import nest.config as config

    class TestDuplication(unittest.TestCase):
        # Add rate in percent to get packet duplicated. 
        def test_duplication(self):
            n0 = Node("n0")
            n1 = Node("n1")
            r = Node("r")
            r.enable_ip_forwarding()

            (n0_r, r_n0) = connect(n0, r)
            (r_n1, n1_r) = connect(r, n1)

            n0_r.set_address("10.1.1.1/24")
            r_n0.set_address("10.1.1.2/24")
            r_n1.set_address("10.1.2.2/24")
            n1_r.set_address("10.1.2.1/24")
            n0.add_route("DEFAULT", n0_r)
            n1.add_route("DEFAULT", n1_r)

            n1_r.set_packet_duplication("20%")

            exp = Experiment("test-packet_duplication")
            flow = Flow(n0, n1, n1_r.address, 0, 5, 2)
            exp.add_tcp_flow(flow)
            exp.run()

            # Add corelation between the duplicate packet
        def test_duplicationCor(self):
            n0 = Node("n0")
            n1 = Node("n1")
            r = Node("r")
            r.enable_ip_forwarding()

            (n0_r, r_n0) = connect(n0, r)
            (r_n1, n1_r) = connect(r, n1)

           n0_r.set_address("10.1.1.1/24")
            r_n0.set_address("10.1.1.2/24")
            r_n1.set_address("10.1.2.2/24")
            n1_r.set_address("10.1.2.1/24")
            n0.add_route("DEFAULT", n0_r)
            n1.add_route("DEFAULT", n1_r)

            n1_r.set_packet_duplication("20%", "5%")

            exp = Experiment("test-packet_duplication")
            flow = Flow(n0, n1, n1_r.address, 0, 5, 2)
            exp.add_tcp_flow(flow)
            exp.run()

        def tearDown(self):
            delete_namespaces()
            TopologyMap.delete_all_mapping()

    if __name__ == "__main__":
        unittest.main()

  ```
 Run the above program with ```sudo``` previlege.
 



### 2. Adding packet reordering   

API for `packett reordering` to be added soon..
This api will be added right after the packet duplication api.


### 3. Adding packet Distribution  

API for `packet distribution` to be added soon..  

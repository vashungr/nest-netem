# Packet Reordering Testing

Packet reorder command is given along with delay. 
```python
 COMMAND:  tc qdisc add dev eth0 root netem delay 10ms reorder 25%
```

#### Problem statement: 
1. Can we add packet reorder alone, provided delay is already in the qdisc.
    ```shell
    tc qdisc change dev eth0 root netem reorder 25%
     ```
2. What happens to the existing delay when we change delay in qdisc along with reorder.  
   ```shell
    tc qdisc change dev eth0 root netem delay 10ms reorder 25%
   ```

To find out the solution to the problem statement. I create a simple network node-to-node topology,   
using namespace and add delay in first node.
1. Add reorder alone without delay, since delay is already specified.  
2. Add reorder along with delay, but we have delay in the qdisc already.  

Lets find out.
### Create a simple network one-to-one using namespace    

```shell
root@vs-linux:/home/shung# ip netns add red
root@vs-linux:/home/shung# ip netns add blue
root@vs-linux:/home/shung# ip netns exec red ip link set dev lo up
root@vs-linux:/home/shung# ip netns exec blue ip link set dev lo up
root@vs-linux:/home/shung# ip link add eth0 type veth peer name eth1 
root@vs-linux:/home/shung# ip link set eth0 netns red
root@vs-linux:/home/shung# ip link set eth1 netns blue
root@vs-linux:/home/shung# ip netns exec red red ip link set dev eth0 up 
root@vs-linux:/home/shung# ip netns exec red ip link set dev eth0 up
root@vs-linux:/home/shung# ip netns exec blue ip link set dev eth1 up
root@vs-linux:/home/shung# ip netns exec red ip address add 192.168.1.1/24 dev eth0
root@vs-linux:/home/shung# ip netns exec blue ip address add 192.168.1.2/24 dev eth1
```

### Add delay on `eth0`

```shell
$ ip netns exec red tc qdisc add dev eth0 root netem delay 100ms
```

### Check for `qdisc` configuration on `eth0`

```shell
$ ip netns exec red tc qdisc show
qdisc noqueue 0: dev lo root refcnt 2 
qdisc netem 8002: dev eth0 root refcnt 2 limit 1000 delay 100.0ms
```
 As  we can see that, there is `delay` of `100.0ms` in the `qdisc`.  

 ### Add reorder without specifying delay

 Without specifying `delay`, because it has `delay` in the `qdisc`.
```shell

$ ip netns exec red tc qdisc change dev eth0 root netem reorder 25%

reordering not possible without specifying some delay
Usage: ... netem	[ limit PACKETS ]
			[ delay TIME [ JITTER [CORRELATION]]]
			[ distribution {uniform|normal|pareto|paretonormal} ]
			[ corrupt PERCENT [CORRELATION]]
			[ duplicate PERCENT [CORRELATION]]
			[ loss random PERCENT [CORRELATION]]
			[ loss state P13 [P31 [P32 [P23 P14]]]
			[ loss gemodel PERCENT [R [1-H [1-K]]]
			[ ecn ]
			[ reorder PERCENT [CORRELATION] [ gap DISTANCE ]]
			[ rate RATE [PACKETOVERHEAD] [CELLSIZE] [CELLOVERHEAD]]
			[ slot MIN_DELAY [MAX_DELAY] [packets MAX_PACKETS] [bytes MAX_BYTES]]
		[ slot distribution {uniform|normal|pareto|paretonormal|custom} DELAY JITTER [packets MAX_PACKETS] [bytes MAX_BYTES]]
```
But, unfortunately, there got a message saying, 
`reordering not possible without specifying some delay`. But we have `delay` already specified in the   
`qdisc`. This means that `reorder` alone is not accepting in the `tc` command.   

Check Next step.


### Add packet reorder with  delay

```shell
$ tc qdisc change dev eth0 root netem delay 10ms reorder 25%
```
Got no problem change `qdisc` to add reorder with `delay`. But what happens to the existing `delay`.  


### Check for `qdisc` Configuration 

```shell
$ tc qdisc show
qdisc noqueue 0: dev lo root refcnt 2 
qdisc netem 8002: dev eth0 root refcnt 2 limit 1000 delay 10.0ms reorder 25% gap 1
```
Delay changed to `10.0ms` from `100ms`.
This means, delay is replaced.


## Conclusion
1. Even we have specified delay in the qdisc, `reorder` alone cannot be given command. It gives a   
message of instruction and do not proceed further.
2. Reorder alone cannot be given command without delay. It has to give command along with delay. 
3. Delay will be replaced by the delay with reorder to the existing delay in qdisc.



```shell
NOTE: I have tested the above problem statement with the classfull qdisc. It gives the same conclusion.
```
